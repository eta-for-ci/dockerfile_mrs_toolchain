FROM ubuntu:20.04

RUN apt-get update
RUN apt-get install -y wget make xz-utils cmake ninja-build python3 python-is-python3
RUN rm -rf /var/lib/apt/lists/*
RUN wget http://file.mounriver.com/tools/MRS_Toolchain_Linux_x64_V1.40.tar.xz -O /tmp/MRS_Toolchain_Linux_x64_V1.40.tar.xz
RUN tar xvf /tmp/MRS_Toolchain_Linux_x64_V1.40.tar.xz -C /opt
RUN rm /tmp/MRS_Toolchain_Linux_x64_V1.40.tar.xz
ENV PATH="$PATH:/opt/MRS_Toolchain_Linux_x64_V1.40/RISC-V Embedded GCC/bin/:/opt/MRS_Toolchain_Linux_x64_V1.40/OpenOCD/bin/"
ENV LD_LIBRARY_PATH=/opt/MRS_Toolchain_Linux_x64_V1.40/beforeinstall/
RUN riscv-none-embed-gcc --version
RUN openocd --version
